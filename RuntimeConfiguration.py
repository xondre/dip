#!/usr/bin/env python3
""" Vytvoření runtime konfigurace systému. Konfigurace startup pouze načte a 
validuje konfigurační soubor, ale stále zůstává limitován formátem JSON.
Konfigurace runtime dynamicky načte jednotlivé moduly, které byly zadány pomocí
slovníku.
"""
__author__ = "Karel Ondřej"


from StartupConfiguration import StartupConfiguration

from parser import BaseParser


# Adresář, kde se nachází implementace modulů pro extrakci vět.
PARSER_MODULE_NAME = "parser"
PARSER_MODULE = __import__(PARSER_MODULE_NAME)
# Adresář, kde se nachází implementace modulů pro zpracování.
MODULE_MODULE_NAME = "module"
MODULE_MODULE = __import__(MODULE_MODULE_NAME)
# Adresář, kde se nachází implementace modulů pro uložení výsledků.
WRITER_MODULE_NAME = "writer"
WRITER_MODULE = __import__(WRITER_MODULE_NAME)


class RuntimeConfiguration(object):
    """ Sestavení konfigurace systému. Oproti 'StartupConfiguration' obsahuje 
    již konkrétní načtené moduly pro jednotlivé fáze zpracování.
    """

    def __init__(self, config):
        """ Vytvoření runtime konfigurace systému.

        :param config: konfigurace
        :type config: StartupConfiguration
        """
        self.input_directory = config["input_directory"]
        self.output_directory = config["output_directory"]
        
        self.parsers = [self.build_parser_instance(prop["class"], prop["kwargs"]) for prop in config["readers"]]
        self.pipeline = [self.build_module_instance(prop["class"], prop["kwargs"]) for prop in config["pipeline"]]
        self.writer = self.build_writer_instance(config["writer"]["class"], config["writer"]["kwargs"])

    def build_parser_instance(self, clazz, kwargs):
        """ Načtení modulu pro extrakci vět. Z adresáře 'PARSER_MODULE' je 
        dynamicky načtena třída 'clazz' a inicializována podle hodnot 'kwargs'.

        :param clazz: název třídy
        :type clazz: str
        :param kwargs: argumenty
        :type kwargs: dict
        :returns: vytvořená a inicializovaná instance třídy 'clazz'
        :rtype: object
        """
        return self.build_class_instance(PARSER_MODULE, clazz, kwargs)

    def build_module_instance(self, clazz, kwargs):
        """ Načtení modulu pro zpracování. Z adresáře 'MODULE_MODULE' je 
        dynamicky načtena třída 'clazz' a inicializována podle hodnot 'kwargs'.

        :param clazz: název třídy
        :type clazz: str
        :param kwargs: argumenty
        :type kwargs: dict
        :returns: vytvořená a inicializovaná instance třídy 'clazz'
        :rtype: object
        """
        return self.build_class_instance(MODULE_MODULE, clazz, kwargs)

    def build_writer_instance(self, clazz, kwargs):
        """ Načtení modulu pro uložení. Z adresáře 'WRITER_MODULE' je 
        dynamicky načtena třída 'clazz' a inicializována podle hodnot 'kwargs'.

        :param clazz: název třídy
        :type clazz: str
        :param kwargs: argumenty
        :type kwargs: dict
        :returns: vytvořená a inicializovaná instance třídy 'clazz'
        :rtype: object
        """
        return self.build_class_instance(WRITER_MODULE, clazz, kwargs)

    def build_class_instance(self, module, name, kwargs):
        """ Načtení modulu ze souboru. Z adresáře adresáře 'module' je 
        dynamicky načtena třída 'clazz' a inicializována podle hodnot 'kwargs'.

        :param module: adresář s modulem 
        :type module: str
        :param clazz: název třídy
        :type clazz: str
        :param kwargs: argumenty
        :type kwargs: dict
        :returns: vytvořená a inicializovaná instance třídy 'clazz'
        :rtype: object
        """
        clazz = getattr(module, name)
        instance = clazz(**kwargs)
        return instance