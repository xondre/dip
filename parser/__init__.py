#!/usr/bin/env python3

__author__ = "Karel Ondřej"


from .BaseParser import BaseParser
from .HTMLParser import HTMLParser
from .MG4JParser import MG4JParser
from .PlainTextParser import PlainTextParser
from .WARCParser import WARCParser