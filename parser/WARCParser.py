#!/usr/bin/env python3
""" Extrakce vět ze souboru ve formátu WARC. Jedná se o kolekci stařených 
webových stránek. Podporované formáty: text a HTML.
"""
__author__ = "Karel Ondřej"


import logging
from warcio.archiveiterator import ArchiveIterator

from . import BaseParser, HTMLParser, PlainTextParser


# logování
LOGGER = logging.getLogger(__name__)


class WARCParser(BaseParser):
    """ Extrakce vět z WARC souboru.
    """
    def __init__(self, gzip=True, **kwargs):
        """ Inicializace.

        :param gzip: archiv je komprimován
        :type gzip: bool
        """
        super(WARCParser, self).__init__(**kwargs)
        
        self.gzip = gzip
        # podporované formáty
        self.parsers = [HTMLParser(), PlainTextParser()]

    def parse(self, filename):
        """ Samotné zpracování ze souboru WARC. Postupně se zpracují jednotlivé 
        záznami. Pro spracování jednoho záznamu se používají ostatní moduly.

        :param filename: název souboru
        :type filename: str
        :returns: Extrahované věty
        :rtype: dict
        """
        with open(filename, "rb") as warc_file:
            for record in ArchiveIterator(warc_file):
                if record.rec_type != 'response':
                    #TODO něco 'loggovat'?
                    continue

                parser = None
                content_type = record.http_headers.get_header('Content-Type')
                target_uri = record.rec_headers.get_header('WARC-Target-URI')

                if not content_type:
                    # nenalezen typ, nelze zvolit parser
                    LOGGER.warn("HTTP header field 'Content-Type' not found.")
                    continue
                
                # na zaklade typu se zvoli parser
                for p in self.parsers:
                    if p.apply_content_type(content_type):
                        parser = p
                
                if parser == None:
                    # nenalezen parser
                    LOGGER.warn("Parser not found for Content-Type: '{}'.".format(content_type))
                    continue

                features = parser.parse(record.content_stream().read(), 
                                        target_uri)

                for sentence in features:
                    yield sentence
    
    def parse_string(self, string, filename=None):
        """ Samotné zpracování formátu WARC z řetězce znaků. Není podporované, 
        jelikož se jedná o kolekci.

        :param string: obsah ke zpracování
        :type string: str
        :param filename: název souboru, pouze pro uvedení původu
        :type filename: str
        :returns: Extrahované věty
        """
        raise NotImplementedError   # nepodporované

    def apply(self, filename):
        return filename.endswith(".warc") or (filename.endswith(".warc.gz") and self.gzip)

    def apply_content_type(self, type):
        raise NotImplementedError