#!/usr/bin/env python3
""" Rozhraní (abstraktní třída) modulů pro extrakci vět z textu.
"""
__author__ = "Karel Ondřej"


import logging
import nltk


# logování
LOGGER = logging.getLogger(__name__)


class BaseParser(object):
    """ Rozhraní modulů pro extrakci vět ze souborů.
    """
    def __init__(self, **kwargs):
        """ Inicializace. """
        pass

    def parse(self, filename, string=None):
        """ Samotné zpracování ze souboru.

        :param filename: název souboru
        :type filename: str
        :returns: Extrahované věty
        """
        raise NotImplementedError

    def parse_string(self, string, filename=None):
        """ Samotné zpracování z řetězce znaků.

        :param string: obsah ke zpracování
        :type string: str
        :param filename: název souboru, pouze pro uvedení původu
        :type filename: str
        :returns: Extrahované věty
        """
        raise NotImplementedError

    def apply(self, filename):
        """ Predikát pro určení, zda lze soubor zpracovat na základě názvu.

        :param filename: název souboru
        :type filename: str
        :returns: True, pokud lze zpracovat, jinak False
        :rtype: bool
        """
        raise NotImplementedError

    def apply_content_type(self, type):
        """ Predikát pro určení, zda lze soubor zpracovat na MIME typu.

        :param type: MIME typ
        :type type: str
        :returns: True, pokud lze zpracovat, jinak False
        :rtype: bool
        """
        raise NotImplementedError

    def sentence_split(self, paragraph):
        """ Rozdělení odstavce na věty.

        :param paragraph: text
        :type paragraph: str
        :returns: sekvence vět
        :rtype: list
        """
        return nltk.sent_tokenize(paragraph)

    def format(self, doc, paragraphId, sentecId, sentence):
        """ Formát výstupu modulů pro extrakci vět.

        :param doc: názec dokumentu
        :type doc: str
        :param paragraphID: identifikátor odstavce
        :type paragraphID: str
        :param sentecID: identifikátor věty
        :type sentecID: str
        :param sentence: extrahovaná věta
        :type sentence: str
        :returns: výstup extrakce obsahujíci jednu větu a informace o původu
        :rtype: dict
        """
        return {
            "doc": doc,
            "parID": paragraphId,
            "senID": sentecId, 
            "sentence": sentence
        }