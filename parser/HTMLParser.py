#!/usr/bin/env python3
""" Extrakce vět ze souborů ve formátu HTML.
"""
__author__ = "Karel Ondřej"


import bs4
import logging

from . import BaseParser


# logování
LOGGER = logging.getLogger(__name__)


class HTMLParser(BaseParser):
    """
    Zpracování stránky HTML, ze které jsou extrahovány věty.
    """
    def __init__(self, parser='html.parser', **kwargs):
        """
        """
        super(HTMLParser, self).__init__(**kwargs)
        self.parser = parser
        LOGGER.info("For parsing is used '{}'".format(self.parser))

    def parse(self, filename):
        """ Samotné zpracování ze souboru.

        :param filename: název souboru
        :type filename: str
        :returns: Extrahované věty
        """
        with open(filename, "r") as f:
            string = f.read()

        self.parse_string(string, filename)

    def parse_string(self, string, filename=None):
        """ Samotné zpracování z řetězce znaků.

        :param string: obsah ke zpracování
        :type string: str
        :param filename: název souboru, pouze pro uvedení původu
        :type filename: str
        :returns: Extrahované věty
        """
        soup = bs4.BeautifulSoup(string, features=self.parser)

        # ignoruj 
        for script in soup(["script", "style"]):
            script.extract()

        text = soup.get_text()
        lines = (line.strip() for line in text.splitlines() if line.strip() != "")

        for parId, line in enumerate(lines):
            for senId, sentence in enumerate(self.sentence_split(line)):
                yield self.format(filename, parId, senId, sentence)

    def apply(self, filename):
        return any(map(lambda suffix: filename.endswith(suffix), ['.html', '.htm']))

    def apply_content_type(self, type):
        return type.startswith("text/html")
