#!/usr/bin/env python3
""" Extrakce vět z formátu MG4J. Jedná se o výstup nástrojů skupiny KnoT
pro zpracování přirozenného jazyka. Blíže v CorpprocDocumentCollection.
"""
__author__ = "Karel Ondřej"


import logging

from . import BaseParser
from .CorprocDocumentCollection import CorpprocDocumentCollection


# logování
LOGGER = logging.getLogger(__name__)


class MG4JParser(BaseParser):
    """ Extrakce vět z formátu MG4J.
    """

    def __init__(self, **kwargs):
        """ Inicializace. 
        """
        super().__init__(**kwargs)

    def parse(self, filename):
        """ Samotné zpracování ze souboru MG4J.

        :param filename: název souboru
        :type filename: str
        :returns: Extrahované věty
        """
        with CorpprocDocumentCollection(filename) as f:
            for document in f:
                for sentence in document.sentences:
                    sentence_string = str(sentence)

                    features = self.format(document.id, sentence.paragraphID, 
                                           sentence.sentenceID, sentence_string)
                    features['tokens'] = sentence.tokens
                    features['entities'] = sentence.entities
                    yield features
    
    def parse_string(self, string, filename=None):
        """ Samotné zpracování z řetězce znaků. Není podporované, jelikož se
        jedná o kolekci.

        :param string: obsah ke zpracování
        :type string: str
        :param filename: název souboru, pouze pro uvedení původu
        :type filename: str
        :returns: Extrahované věty
        """
        raise NotImplementedError   # nepodporované

    def apply(self, filename):
        return filename.endswith(".mg4j")

    def apply_content_type(self, type):
        return False
