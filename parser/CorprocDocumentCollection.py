#!/usr/bin/env python3
""" Zpracování kolekce dokumnetů z formátu MG4J. Jedná se o výstupní formát
nástrojů (Corpproc) pro zpracování přirozeného jazyka výskupné skupiny KnoT.
"""
__author__ = "Karel Ondřej"


import sys
import logging


LOGGER = logging.getLogger(__name__)

POSITION_COLUMN_INDEX  = 0      # index sloupce pro pozici slova ve větě
TOKEN_COLUMN_INDEX     = 1      # index sloupce pro token


class CorpprocIndexedWord:
    """ Jedno zaindexované slovo v kolekci. Každé slovo k sobě má další 
    informace ze zpracování.
    """
    # položky, které obsahují číselné hodnoty
    INT_LABELS = ['position', 'length', 'nerlength', 'parpos', 'paroffset']
    REQ_LABELS = ['token']

    def __init__(self, labels, values, glue=False):
        """ Inicializace.

        :param labels: názvy jednotlivých položek
        :type labels: list
        :param values: hodnoty jednotlivých položek
        :type values: list
        :param glue: Má být přidružený k předchozímu slovu? Pokud True, nemá být
                     mezera mezi ním a předchozím slovem.
        :type glue: bool
        """
        self.token = ''
        self.glue = glue

        for label, value in zip(labels, values):
            if label in self.INT_LABELS:
                value = int(value)
            elif label not in self.REQ_LABELS:
                value = None if value == '0' else value
            setattr(self, label, value)

        #LOGGER.debug(self.__dict__)

    def __str__(self):
        """ Reprezentace tokenu jako řetězec.
        """
        return str(self.token)


class CorpprocSentence(object):
    """ Reprezentace zaindexované věty.
    """
    def __init__(self, words = None, documentID = None, paragraphID = None, sentenceID = None):
        """ Inicializace.

        :param words: seznam zaindexovaných slov
        :type words: list
        :param documentID: identifikátor dokumentu, kde se věta nachází
        :type documentID: str
        :param paragraphID: identifikátor odstavce, kde se věta nachází
        :type paragraphID: str
        :param sentenceID: identifikátor věty
        :type sentenceID: str
        """
        self.words = words if words else []
        self.document = documentID
        self.paragraphID = paragraphID
        self.sentenceID = sentenceID

    def __str__(self):
        """ Reprezentace věty jako řetězec.
        """
        try:
            sentence = " ".join([word.token + ('|G__' if word.glue else '') for word in self.words])
            sentence = sentence.replace('|G__ ', '')
        except:
            LOGGER.error(self.document.id)
            LOGGER.error(str([word.token for word in self.words]))
        return sentence

    @property
    def tokens(self):
        """ Seznam tokenů jednotlivých slov. 
        
        :returns: seznam tokenů
        :rtype: list
        """
        return [word.token for word in self.words]

    @property
    def entities(self):
        """ Vygeneruje seznam entit a jejich pozice ve větě. 

        :returns: seznam entit
        :rtype: list
        """
        def is_float(num):
            try:
                float(num)
                return True
            except:
                return False

        ner = [(word.position-word.nerlength, word.position, word.nertag) for word in self.words if word.nertag!=None]
        wiki_mode = [(word.position-word.length, word.position, word.docuri) for word in self.words if word.docuri and not is_float(word.docuri)]

        return sorted(ner + wiki_mode, key=lambda t: t[0])


class CorpprocDocument(object):
    """ Reprezenatce jednoho dokomentu.
    """
    def __init__(self, name=None, id_=None, label="", link=""):
        """ Inicializace.

        :param name: název dokumentu
        :type name: str
        :param id_: identifikátor dokumentu
        :type id_: str
        :param label: titulek webové stránky
        :type label: str
        :param link: odkaz webové stránky
        :type link: str
        """
        self.name = name
        self.id = id_
        self.label = label
        self.link = link
        self.sentences = []
        self.title = CorpprocSentence()


class CorpprocDocumentCollection(object):
    """ Zpracování kolekce dokumentů ve formátu MG4J.
    """

    METADATA_TAG = r"%%#"   # uvozovaci sekvence matatagu
    DOCUMENT_TAG = "DOC"    # informace od dokumentu
    PAGE_TAG = "PAGE"       # informace o stránce
    PARAGRAPH_TAG = "PAR"   # informace o odstavci
    SENTENCE_TAG = "SEN"    # informace o větě

    def __init__(self, filename):
        """ Indicializace.

        :param filename: nazev a cesta k souboru ke zpracování
        :type filename: str
        """
        self.filename = filename
        self.fd = None
    
    def open(self):
        """ Otevření souboru a uložení desktiptoru do stavu objektu. Pokud je již
        otevřen, nic se nestane. Použito ve funkci '__enter__'.
        """
        if not self.fd:
            self.fd = open(self.filename, "r")

    def close(self):
        """ Zavření otevřeného souboru. Pokud nebyl soubor otevřen, nebo by již
        zavřen, nic se nestane. Použito ve funkci '__exit__'.
        """
        if self.fd:
            self.fd.close()
            self.fd = None

    def __iter__(self):
        """ Vytvoření iterátoru (generátoru), který vrací jednotlivé zpracované
        dokumenty z kolekce

        :returns: iterátor přes dokumenty kolekce
        :rtype: iter
        """
        return self.nextDocument()

    def nextDocument(self):
        """ Generátor pro procházení dokumentů v kolekci. Každé zavolání
        zpracuje a vrátí jeden dokument z kolekce.

        :returns: iterátor přes dokumenty kolekce
        :rtype: iter
        """
        document = None
        sentence = None
        paragraph = None

        self.fd.readline()

        labels = self.fd.readline().strip().split()

        for line in self.fd:
            line = line.strip()
            if line.startswith(self.METADATA_TAG):
                # jedná se o matatag
                metadata = line[len(self.METADATA_TAG):].split()
                if metadata[0] == self.DOCUMENT_TAG:
                    # informace o dokumentu
                    if document != None:
                        # vrať předchozí dokument
                        yield document
                    # nový dokument
                    document = CorpprocDocument('', metadata[1])
                elif metadata[0] == self.PAGE_TAG:
                    # informace o webové stránce
                    #TODO podivny format, dohledat
                    document.label = " ".join(metadata[1:-1])
                    document.link = metadata[-1]
                elif metadata[0] == self.PARAGRAPH_TAG:
                    # informace o odstavci
                    paragraph = metadata[2]
                elif metadata[0] == self.SENTENCE_TAG:
                    # informace o větě
                    sentence = CorpprocSentence([], document, paragraph, metadata[2])
                    document.sentences.append(sentence)
                else:
                    LOGGER.error("Unknown metadata tag '{}{}'".format(self.METADATA_TAG, metadata[0]))
            else:
                # indexovane slova
                cols = line.split()
                glue = cols[TOKEN_COLUMN_INDEX].endswith('|G__')
                cols = [col.strip('|G__') for col in cols]

                word = CorpprocIndexedWord(labels=labels, values=cols)

                if sentence:
                    # slovo ve větě
                    if glue:
                        sentence.words[-1].glue = True
                    sentence.words.append(word)
                else:
                    # slovo v titulku stranky
                    if glue:
                        document.title.words[-1].glue = True
                    document.title.words.append(word)

        if document != None:
            yield document

    def __enter__(self):
        """
        """
        self.open()
        return self

    def __exit__(self, type, value, traceback):
        self.close()


if __name__ == "__main__":
    filename = sys.argv[1]
    with CorpprocDocumentCollection(filename) as doc:
        for document in doc.nextDocument():
            print("Title: " + str(document.title))
            print("Label: " + document.label)
            print("Link: " + document.link)
            for sentence in document.sentences:
                print(sentence)
