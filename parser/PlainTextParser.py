#!/usr/bin/env python3
""" Extrakce vět z prostého textu.
"""
__author__ = "Karel Ondřej"


import logging

from . import BaseParser


# logování
LOGGER = logging.getLogger(__name__)


class PlainTextParser(BaseParser):
    """ Extrakce vět z prostého textu.
    """

    def __init__(self, **kwargs):
        """ Inicializace. 
        """
        super(PlainTextParser).__init__()

    def parse(self, filename):
        """ Samotné zpracování z textového souboru.

        :param filename: název souboru
        :type filename: str
        :returns: Extrahované věty
        """
        with open(filename, "r") as f:
            text = f.read()

        self.parse_string(text, filename)

    def parse_string(self, string, filename=None):
        """ Extrakce věty z řetězce znaků.

        :param string: obsah ke zpracování
        :type string: str
        :param filename: název souboru, pouze pro uvedení původu
        :type filename: str
        :returns: Extrahované věty
        """
        sentences = self.sentence_split(string)

        for idx, sentence in enumerate(sentences):
            yield self.format(filename, 0, idx, sentence)

    def apply(self, filename):
        return filename.endswith(".txt")

    def apply_content_type(self, type):
        return type.startswith("text/plain")

