#!/usr/bin/env python3
""" Zpracování vstupu pomocí nástroje SpaCy. Hlavně získání pojmenovaných entit.
"""
__author__ = "Karel Ondřej"


import logging
import spacy

from . import BaseModule


# logování
LOGGER = logging.getLogger(__name__)


class SpaCyPipeline(BaseModule):
    """ Extrakce pojmenovaných entit založená na spacy.
    """
    def __init__(self, model="en_core_web_sm", **kwargs):
        """ Inicializace.

        :param model: předtrénovaný model spacy pro zpracování
        :type model: str
        """
        LOGGER.info("Load model '{}'.".format(model))
        self.nlp = spacy.load(model)

    def run(self, features):
        """ Extrakce entit. Pokud již byly entity rozpoznány (např. byly 
        načteny už ze vstupního souboru), nedojde k extrakci.

        :param features: vstupní věty a další informace.
        :type features: generator
        """
        for doc in features:
            if 'entities' not in doc:
                sentence = self.nlp(doc["sentence"])
                tokens = [token.text for token in sentence]
                entities = [(entity.start, entity.end, entity.label_) for entity in sentence.ents]
                doc["tokens"] = tokens
                doc["entities"] = entities
            yield doc
