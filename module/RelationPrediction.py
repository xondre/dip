#!/usr/bin/env python3
""" Modul pro predikci vztahů mezi pojmenovanými entitami.
"""
__author__ = "Karel Ondřej"


import itertools
import logging
import torch
import sys

sys.path.insert(0, 'lib/remodel')    # TODO funguje, ale musí se předělat
from . import BaseModule
from lib.remodel.framework import RelationExtractionFramework
from lib.remodel import *
from lib.remodel.model import FeaturesConverter


LOGGER = logging.getLogger(__name__)


class EntityPairDataset(torch.utils.data.IterableDataset):
    """ Vytvoření vstupu pro klasifikaci vztahů. """
    def __init__(self, sentence, features_fn):
        """ Inicializace.

        :param sentence: Vstupní věta a další informace.
        :type sentence: dict
        :param features_fn: funkce pro vytvoření formátu pro klasifikaci
        :type features_fn: function
        """
        super(EntityPairDataset).__init__()
        self.sentence = sentence
        self.features_fn = features_fn

    def __iter__(self):
        """ Vytvoření vstupu pro klasifikaci pomocí modelu. Z jednotlivých entit
        ve větě se vygenerují komvinace dvojic a převede se na požadovaný formát.

        A <e1> entita 1 </e1> B <e2> entita 2 </e2> C .

        :returns: generátor dvojic s položkami ve formátu vhodném pro klasifikaci.
        """

        for comb in itertools.combinations(self.sentence["entities"], 2):
            LOGGER.debug("Entity combination: " + str(comb))
            se1, ee1, _ = comb[0]   # začátek a konec entity 1
            se2, ee2, _ = comb[1]   # začátek a konec entity 1
            tokens = self.sentence["tokens"]
            # A <e1> entita 1 </e1> B <e2> entita 2 </e2> C .
            tokens = tokens[:se1] + ["<e1>"] + tokens[se1:ee1+1] + ["</e1>"] + tokens[ee1+1:se2] + ["<e2>"] + tokens[se2:ee2+1] + ["</e2>"] + tokens[ee2+1:]
            task = {"sentence": " ".join(tokens)}
            yield self.features_fn(task)


class RelationPrediction(BaseModule):
    """ Predikce vztahu mezi pojmenovanými entitami.
    """
    def __init__(self, model_path, **kwargs):
        """ Inicializace.

        :param model_path: cesta k naučenému modelu
        :type model_path: str
        """
        LOGGER.debug("Load model: '{}'.".format(model_path))
        self.model = torch.load(model_path)
        self.features_fn = FeaturesConverter(label_list=self.model.labels, **kwargs).convert_task_to_features
        self.framework = RelationExtractionFramework(self.model, **kwargs)      

    def run(self, features):
        """ Postupně se v každé větě mezi každou kombinací dvojice entit 
        určí vztah.

        :param features: věty a pojmenované entity
        :type features: generator
        """
        for sentence in features:
            if 'entities' not in sentence:
                LOGGER.error("Named entities recognize is missed.")
                raise Exception("Named entities recognize is missed.")

            relationships = []
            if len(sentence["entities"]) >= 2:
                dataset = EntityPairDataset(sentence, self.features_fn)
                predictions = self.framework.predict(dataset)
                combinations = itertools.combinations(range(len(sentence["entities"])), 2)
                rels = [(comb[0], comb[1], self.model.labels[prediction["predicted_class"]]) for prediction, comb in zip(predictions, combinations)]
                # spravný směr a odstranění směru z názvu
                rels = [(o, s, r[:-7]) if r.endswith("(e2,e1)") else (s, o, r[:-7]) for s, o, r in rels if r.endswith(",e2)") or r.endswith(",e1)")]
                relationships.extend(rels)
            sentence["relationships"] = relationships

            yield sentence
