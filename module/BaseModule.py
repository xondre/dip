#!/usr/bin/env python3
""" Rozhraní modulů pro zpracování.
"""
__author__ = "Karel Ondřej"


import logging


LOGGER = logging.getLogger(__name__)


class BaseModule(object):
    """ Rozhraní modulů pro zpracování.
    """
    def __init__(self, **kwargs):
        """ Inicializace. """
        pass

    def run(self, features):
        """ Zpracování vstupní věty. Každý modul vrací přijímá a vrací generátor.
        Postupně generuje jednotlivé výsledky předchozího zpracování a 
        vytváří nový generátor s přidanými vlastními výsledky.

        :param features: generátor jednotlivých výsledků zpracování
        :type features: generator
        """
        raise NotImplementedError
