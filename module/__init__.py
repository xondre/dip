#!/usr/bin/env python3

__author__ = "Karel Ondřej"


from .BaseModule import BaseModule
from .SpaCyPipeline import SpaCyPipeline
from .RelationPrediction import RelationPrediction