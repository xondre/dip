#!/usr/bin/env python3
""" Modul pro načtení a validaci konfiguračního souboru. Konfigurační soubor
je ve formátu JSON a pro jeho validaci se využívá JSON scheme (resp. jeho 
alternativa pro slovníky v python). 
"""
__author__ = "Karel Ondřej"


import jsonschema
import json


""" Vychozi schema pro modul. Vlastnost 'class' odpovida nazvu modelu, 
a 'kwargs' slovnik argumentu pro konstruktor.
"""
DEFAULT_CLASS_SCHEMA = {
    'type': 'object',
    'properties': {
        'class': {
            'type': 'string'
        },
        'kwargs': {
            'type': 'object'
        }
    },
    "required": ["class"],
    "additionalProperties": False
}

# Schéma modulu pro extrakci vět
READER_SCHEMA = DEFAULT_CLASS_SCHEMA
# Schéma modulu pro zpracování
PARSER_SCHEMA = DEFAULT_CLASS_SCHEMA
# Schéma modulu pro uložení zpracování
WRITER_SCHEMA = DEFAULT_CLASS_SCHEMA

# Schéma konfiguračního souboru.
CONFIG_SCHEMA = {
    'type': 'object',
    'properties': {
        'readers': {
            'type': 'array',
            'minItems': 1,
            'items': READER_SCHEMA
        },
        'pipeline': {
            'type': 'array',
            'items': PARSER_SCHEMA
        },
        'writer': WRITER_SCHEMA,
        'input_directory': {
            'type': 'string'
        },
        'output_directory': {
            'type': 'string'
        },
    },
    'required': ['readers', 'pipeline', 'input_directory']
}


class StartupConfiguration(object):
    """
    Zpracování a validace vstupního konfiguračního souboru. Třída vstupní
    konfigurační soubor načte, následně validuje jeho strukturu a ještě doplní
    výchozí hodnoty pro nepovinné parametry. Pomocí instance třídy lze
    přistupovat k jednotlivým položkám konfiguračního souboru.
    """

    def __init__(self, config_file):
        """ Načtení a zpracování.

        :param config_file: Název konfiguračního souboru.
        :type config_file: str
        """
        self.config = self.load_config_file(config_file)
        self.validate(self.config)
        self.fix_readers()
        self.fix_pipeline()
        self.add_default_keywords_arguments(self.config["writer"])

    def __getitem__(self, key):
        """ Přístup k položkám konfigurace.

        :param key: Název položky.
        :type key: str
        :returns: Hodnotu položky.
        """
        return self.config[key]

    def validate(self, config):
        """ Validace konfiguračního souboru podle JSON scheme.

        :param config: Načtený konfigurační soubor ve formátu slovníku.
        :type config: dict
        """
        jsonschema.validate(config, CONFIG_SCHEMA)

    def load_config_file(self, filename):
        """ Načtení konfigurace ze souboru.

        :param filename: Název konfiguračního souboru.
        :type filename: str
        :returns: Načtený konfigurační soubor ve formátu slovníku.
        :rtype: dict
        """
        with open(filename) as f:
            config = json.load(f)

        return config

    def fix_readers(self):
        """ Validace konfigurace modulů pro extrakci vět. Pokud neobsahuje
        struktura položku 'kwargs', tak je inicializována na prázdný slovník.
        """
        for reader in self.config["readers"]:
            self.add_default_keywords_arguments(reader)

    def fix_pipeline(self):
        """ Validace konfigurace modulů pro zpracování přirozeného jazyka. 
        Pokud neobsahuje struktura položku 'kwargs', tak je inicializována na 
        prázdný slovník.
        """
        for reader in self.config["pipeline"]:
            self.add_default_keywords_arguments(reader)

    def add_default_keywords_arguments(self, clazz):
        """ Přidání položky 'kwargs' inicializované ná prázdný slovník.

        :param clazz: struktura reprezentujici modul.
        :type clazz: dict
        """
        if "kwargs" not in clazz:
            clazz["kwargs"] = {}
