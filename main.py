#!/usr/bin/env python3
""" Hlavní smyčka programu. Systém nejdříve zpracuje a validuje konfigurační 
soubor. Následně dynamicky importuje moduly z konfiguračního souboru. 
V hlavní smyčce se cyklicky pro všechny soubory v vstupním adresáři provede:
1. Výběr modulu pro extrakci vět.
2. Postupné aplikování jednotlivých modulů pro zpracování.
3. Uložení výsledků do souboru
"""
__author__ = "Karel Ondřej"


import logging
import os

from StartupConfiguration import StartupConfiguration
from RuntimeConfiguration import RuntimeConfiguration


# Logování
LOGGER = logging.getLogger(__name__)


def build_parser():
    """ Sestavení parseru pro zpracování argumentů z konzole.

    :returns: parser pro zpracování argumentů
    :rtype: object
    """
    import argparse
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--config', default="config.json")
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--log', default='')
    return parser


def main(args):
    """ Hlavní smyčka programu 

    :param args: zpracované argumenty programu
    :type args: dict
    """
    # načtení a validace konfiguračního souboru 
    startupConfig = StartupConfiguration(args.config)
    # dynamické importování modulů
    runtimeConfig = RuntimeConfiguration(startupConfig)

    for filename in os.listdir(runtimeConfig.input_directory):
        features = {}   # výsledky zpracování
        parser = None
        for p in runtimeConfig.parsers:
            if p.apply(filename):
                parser = p
     
        path = os.path.join(runtimeConfig.input_directory, filename)

        if parser == None:
            # nepodporovaný formát
            LOGGER.warning("Parser does not found for file: '{}'.".format(path))
            continue

        # sestavení generátoru
        features = parser.parse(path)        
        try:
            for module in runtimeConfig.pipeline:
                features = module.run(features)
        except Exception as e: 
            LOGGER.error('Something went wrong during processing.\n'+str(e))
            continue

        # uložení do souboru
        runtimeConfig.writer.write(runtimeConfig.output_directory,
                                   filename,
                                   features)


if __name__ == "__main__":
    parser = build_parser()
    args = parser.parse_args()

    if args.log:
        logging.config.fileConfig(args.log)
    else:
        logging.basicConfig(filename='relation-extraction.log',level=logging.DEBUG if args.debug else logging.INFO)

    main(args)