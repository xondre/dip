#!/usr/bin/env python3
""" Uložení výsledků extrakce na standartní výstup. Vhodné pro debugování
systému.
"""
__author__ = "Karel Ondřej"


from . import BaseWriter


class StandartOutputWriter(BaseWriter):
    """ Uložení výsledků zpracování na standartní výstup.
    """
    def __init__(self, **kwargs):
        """ Inicilaizace. 
        """
        super(StandartOutputWriter, self).__init__(**kwargs)

    def write(self, directory, filename, features):
        """ Uložení výsledků na standartní výstup. Adresář je ignorován a název 
        souboru slouží k identifinaci zdrojového souboru.

        :param directory: název adresáře (ignorován)
        :type directory: str
        :param filename: název souboru
        :type filename: str
        :param features: výsledky zpracování
        :type features: iter
        """
        for sentence in features:
            print("{}\t{}".format(filename, sentence))