#!/usr/bin/env python3

__author__ = "Karel Ondřej"


from .BaseWriter import BaseWriter
from .StandartOutputWriter import StandartOutputWriter
from .TripleWriter import TripleWriter