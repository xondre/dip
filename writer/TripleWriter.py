#!/usr/bin/env python3
""" Uložení výsledku extrakce vztahů ve formátu N-Triples.
"""
__author__ = "Karel Ondřej"


import os

from . import BaseWriter


class TripleWriter(BaseWriter):
    """ Uložení výsledku extrakce vztahů ve formátu N-Triples.
    """
    def __init__(self, **kwargs):
        """ Inicializace. """
        super(TripleWriter, self).__init__(**kwargs)

    def write(self, directory, filename, features):
        """ Uložení výsledku extrakce vztahů do souboru ve formátu N-Triples. 
        Na samostatném řádku se nachází jeden vztah ve tvaru

        <podmět> <predikát> <předmět> .

        kde predikát je název vztahu, podmět a předmět pojmenované entity.

        :param directory: název adresáře, kde se vytvoří nový soubor
        :type directory: str
        :param filename: název souboru
        :type filename: str
        :param features: výsledky zpracování ve formátu slovníku
        :type features: iter
        """
        path = os.path.join(directory, filename+".ts")

        with open(path, "w") as f:
            for sentence in features:
                for e1, e2, relation in sentence["relationships"]:
                    e1s, e1e, e1type = sentence["entities"][e1]
                    e2s, e2e, e2type = sentence["entities"][e2]
                    subject = " ".join(sentence["tokens"][e1s:e1e])
                    object = " ".join(sentence["tokens"][e2s:e2e])
                    subject = '{}:"{}"'.format(e1type[:3].lower(), subject)
                    object = '{}:"{}"'.format(e2type[:3].lower(), object)
                    triple = self.triple_format(relation, subject, object)
                    f.write(triple)

    def triple_format(self, relation, subject, object):
        """ Formát řádku N-Triples:
        <podmět> <predikát> <předmět> .

        :param relation: vztah, neboli predikát
        :type relation: str
        :param subject: první entita, neboli podmět
        :type subject: str
        :param object: druhá entita, neboli předmět
        :type object: str
        :returns: formát řádku N-Triples
        :rtype: str
        """
        return '{}\t{}\t{}\t.\n'.format(subject, relation, object)