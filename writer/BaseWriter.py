#!/usr/bin/env python3
""" Rozhraní modulů pro uložení.
"""
__author__ = "Karel Ondřej"


class BaseWriter(object):
    """ Rothraní modulů pro uložení výsledků zpracování.
    """
    def write(self, directory, filename, features):
        """ Uložení výsledků zpracování do souboru.

        :param directory: adresář pro uložení
        :type directory: str
        :param filename: název souboru pro zpracování
        :type filename: str
        :param features: iterátor s výsledky zpracování
        :type features: iter
        """
        raise NotImplementedError