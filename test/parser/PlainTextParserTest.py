#!/usr/bin/env python3

import unittest

from parser import PlainTextParser
from .Base import FILES, CONTENT_TYPES


TEXT = """Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
Aliquam id dolor. 
Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien. 
Nullam feugiat, turpis at pulvinar vulputate, erat libero tristique tellus, nec bibendum odio risus sit amet ante. 
Etiam posuere lacus quis dolor. Nunc auctor. 
Nulla non arcu lacinia neque faucibus fringilla. Etiam egestas wisi a erat. 
Praesent in mauris eu tortor porttitor accumsan. 

Donec quis nibh at felis congue commodo. 
Praesent id justo in neque elementum ultrices. 
Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. 
Curabitur bibendum justo non orci. Mauris elementum mauris vitae tortor. 
Aliquam erat volutpat. Integer imperdiet lectus quis justo."""

FILE = 'example.txt'

SENTENCE = [
    {'doc': FILE, 'parID': 0, 'senID': 0, 'sentence': 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.'},
    {'doc': FILE, 'parID': 0, 'senID': 1, 'sentence': 'Aliquam id dolor.'},
    {'doc': FILE, 'parID': 0, 'senID': 2, 'sentence': 'Curabitur ligula sapien, pulvinar a vestibulum quis, facilisis vel sapien.'},
    {'doc': FILE, 'parID': 0, 'senID': 3, 'sentence': 'Nullam feugiat, turpis at pulvinar vulputate, erat libero tristique tellus, nec bibendum odio risus sit amet ante.'},
    {'doc': FILE, 'parID': 0, 'senID': 4, 'sentence': 'Etiam posuere lacus quis dolor.'},
    {'doc': FILE, 'parID': 0, 'senID': 5, 'sentence': 'Nunc auctor.'},
    {'doc': FILE, 'parID': 0, 'senID': 6, 'sentence': 'Nulla non arcu lacinia neque faucibus fringilla.'},
    {'doc': FILE, 'parID': 0, 'senID': 7, 'sentence': 'Etiam egestas wisi a erat.'},
    {'doc': FILE, 'parID': 0, 'senID': 8, 'sentence': 'Praesent in mauris eu tortor porttitor accumsan.'},
    {'doc': FILE, 'parID': 0, 'senID': 9, 'sentence': 'Donec quis nibh at felis congue commodo.'},
    {'doc': FILE, 'parID': 0, 'senID': 10, 'sentence': 'Praesent id justo in neque elementum ultrices.'},
    {'doc': FILE, 'parID': 0, 'senID': 11, 'sentence': 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.'},
    {'doc': FILE, 'parID': 0, 'senID': 12, 'sentence': 'Curabitur bibendum justo non orci.'},
    {'doc': FILE, 'parID': 0, 'senID': 13, 'sentence': 'Mauris elementum mauris vitae tortor.'},
    {'doc': FILE, 'parID': 0, 'senID': 14, 'sentence': 'Aliquam erat volutpat.'},
    {'doc': FILE, 'parID': 0, 'senID': 15, 'sentence': 'Integer imperdiet lectus quis justo.'}
]


class PlainTextParserTest(unittest.TestCase):

    def test_apply(self):
        parser = PlainTextParser()
        results = [parser.apply(file) for file in FILES]
        self.assertEqual(results, [False, False, False, False, False, True])

    def test_apply_content_type(self):
        parser = PlainTextParser()
        results = [parser.apply_content_type(type_) for type_ in CONTENT_TYPES]
        self.assertEqual(results, [False, False, False, False, False, True, True])

    def test_parse_text_string(self):
        parser = PlainTextParser()
        result = list(parser.parse(FILE, TEXT))
        self.assertEqual(result, SENTENCE)


if __name__ == '__main__':
    unittest.main()