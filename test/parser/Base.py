#!/usr/bin/env python3

FILES = [
    'example.html',
    'example.htm',
    'example.js',
    'example.css',
    'example.gif',
    'example.txt'
]

CONTENT_TYPES = [
    'text/html',
    'text/html;charset=UTF-8',
    'text/javascript',
    'text/css',
    'image/gif',
    'text/plain',
    'text/plain;charset=UTF-8',
]