#!/usr/bin/env python3

__author__ = "Karel Ondřej"

from .HTMLParserTest import HTMLParserTest
from .PlainTextParserTest import PlainTextParserTest
from .MG4JParserTest import MG4JParserTest