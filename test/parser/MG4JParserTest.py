#!/usr/bin/env python3

import unittest

from .Base import FILES, CONTENT_TYPES
from parser import MG4JParser

FILE = "8a75b674-e6db-536b-9c75-c7d6f4525995"

SENTENCE = [
    {'doc': FILE, 'parID': 'wx1', 'senID': 'wx1', 'sentence': "!!", 'tokens': ['!', '!'], 'entities': []},
    {'doc': FILE, 'parID': 'wx1', 'senID': 'wx2', 'sentence': "!", 'tokens': ['!'], 'entities': []},
    {'doc': FILE, 'parID': 'wx2', 'senID': 'wx3', 'sentence': "!!", 'tokens': ['!', '!'], 'entities': []},
    {'doc': FILE, 'parID': 'wx2', 'senID': 'wx4', 'sentence': "!", 'tokens': ['!'], 'entities': []},
    {'doc': FILE, 'parID': 'wx3', 'senID': 'wx5', 'sentence': "!!!", 'tokens': ['!', '!', '!'], 'entities': []},
    {'doc': FILE, 'parID': 'wx3', 'senID': 'wx6', 'sentence': "v roce 2007", 'tokens': ['v', 'roce', '2007'], 'entities': [(2, 3, 'date')]},
    {'doc': FILE, 'parID': 'wx4', 'senID': 'wx7', 'sentence': "Základní informace", 'tokens': ['Základní', 'informace'], 'entities': []},
    {'doc': FILE, 'parID': 'wx5', 'senID': 'wx8', 'sentence': "Původ", 'tokens': ['Původ'], 'entities': []},
    {'doc': FILE, 'parID': 'wx6', 'senID': 'wx9', 'sentence': "Sacramento , Kalifornie , USA", 'tokens': ['Sacramento', ',', 'Kalifornie', ',', 'USA'], 'entities': [(0, 1, 'settlement'), (4, 5, 'country')]},
]


class MG4JParserTest(unittest.TestCase):

    def test_apply(self):
        parser = MG4JParser()
        results = [parser.apply(file) for file in FILES]
        self.assertEqual(results, [False, False, False, False, False, False])

    def test_apply_content_type(self):
        parser = MG4JParser()
        results = [parser.apply_content_type(type_) for type_ in CONTENT_TYPES]
        self.assertEqual(results, [False, False, False, False, False, False, False])

    def test_parse_file(self):
        parser = MG4JParser()
        result = list(parser.parse("data/example.mg4j"))
        self.assertEqual(result, SENTENCE)


if __name__ == '__main__':
    unittest.main()